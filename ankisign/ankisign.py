#!/usr/bin/env nix-shell
#!nix-shell --pure -i python3 -p "python3.withPackages (ps: with ps; [ genanki requests beautifulsoup4 ])" ffmpeg
from bs4 import BeautifulSoup
import requests
import subprocess
import genanki
import shutil
import json
import sys
import re
import os

import argparse

# providers
from .providers.tegnapp import Tegnapp
from .providers.tegnsprog import Tegnsprog
from .sign import Sign
from .wordlist import Wordlist
from .deck import Deck

import logging

logging.basicConfig()

log = logging.getLogger("ankisign")
log.setLevel(logging.DEBUG)


class AnkiSign(object):
    def __init__(self, providers, wordlist_path: str, output_path: str, cache_dir: str):
        # providers, will prefer the one added first
        self.providers = providers

        # output path for our deck
        self._output_path = output_path

        # ensure cache path exists
        cache_dir_expanded = os.path.expanduser(cache_dir)
        if not os.path.exists(cache_dir_expanded):
            os.makedirs(cache_dir_expanded)
        self._cache_dir = cache_dir_expanded

        # parse wordlist
        self._wordlist = Wordlist(wordlist_path)
        self._wordlist.parse()

    def search(self, search_word: str, strict: bool = False) -> list[Sign]:
        signs = []
        for p in self.providers:
            found_signs = p.search(search_word, strict)
            if not found_signs:
                log.info(f"no results for {search_word} in provider {p.name()}")
                continue

            signs.extend(found_signs)

        return self._remove_dups(signs)

    def generate_deck(self) -> str:
        signs = []
        for word in self._wordlist.words:
            found_signs = self.search(word.search_term, self._wordlist.strict)
            if not found_signs:
                continue

            # change the meaning to the meaning of the word, not the seach term
            found_signs = self._change_meaning_sings(found_signs, word.word)

            signs.append(found_signs[0])

        deck = Deck(
            deck_id=self._wordlist.deck_id,
            deck_title=self._wordlist.deck_title,
            signs=signs,
            cache_dir=self._cache_dir,
        )

        return deck.generate(self._output_path)

    def _change_meaning_sings(self, signs: list[Sign], new_meaning: str) -> list[Sign]:
        for sign in signs:
            sign.meaning = new_meaning
        return signs

    def _check_in(self, check_sign: Sign, sign_list: list[Sign]):
        for sign in sign_list:
            if check_sign.meaning == sign.meaning:
                return True

        return False

    def _remove_dups(self, signs: list[Sign]):
        new_signs = []

        for sign in signs:
            if not self._check_in(sign, new_signs):
                new_signs.append(sign)

        return new_signs


parser = argparse.ArgumentParser()
parser.add_argument("--wordlist", type=str, required=True)
parser.add_argument("--output", type=str)
parser.add_argument("--cache-dir", type=str, default="~/.cache/ankisign")
args = parser.parse_args()

# if no output file specified, default to wordlist name
if not args.output:
    args.output = os.path.basename(args.wordlist).rsplit(".", 1)[0]

# init AnkiSign
providers = [
    # Tegnapp(),
    Tegnsprog(),
]

asign = AnkiSign(providers, args.wordlist, args.output, cache_dir=args.cache_dir)

# generate our deck
output_path = asign.generate_deck()
print(f"Generated deck at {output_path}")
