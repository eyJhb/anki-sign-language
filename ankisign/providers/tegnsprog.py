import json
import os
import re

import requests
from bs4 import BeautifulSoup

from ..sign import Sign

from typing import Any, Optional

import logging

log = logging.getLogger(__name__)

BASE_URL = "https://www.tegnsprog.dk"

TEGNSPROG_DICTS = [
    # "emne",
    # "artikulationssted",
    "artikel",
    "aekvivalent",
    "saetningsord",
    "produktion",
    # "haandform",
    # "aekvivalent_hel",
    # "glosse",
    # "artikel_liste",
    # "aekvivalent_boejningsform",
    # "saetning",
]


class Tegnsprog(object):
    def __init__(self, cache_dir: str = "~/.cache/tegnsprog"):
        # requests session
        self.s = requests.Session()
        self.s.headers.update(
            {
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:106.0) Gecko/20100101 Firefox/106.0"
            }
        )

        # cache
        self._cache_dir = os.path.expanduser(cache_dir)

        # keep our dict cache in here
        self._dicts: dict[str, Any] = {}

    def name(self):
        return "Tegnsprog"

    def search(self, word: str, strict: bool = False):
        word_lower = word.lower()

        # strict/not strict regex
        if strict:
            regex_string = r"(^%s(~\d)?$)" % (word_lower)
        else:
            regex_string = r"(^{0} | {0}$| {0} |^{0}(~\d)?$)".format(word_lower)

        re_word = re.compile(regex_string, re.UNICODE)

        # load dicts and search them stupidly
        dicts = self._load_dicts()

        log.debug(f"searching for word '{word_lower}' with regex '{regex_string}'")

        signs = []
        for dict_name, dict_words in dicts.items():
            for word_dict in dict_words:
                current_word_lower = word_dict["word"].lower()
                if re_word.match(current_word_lower):
                    video_url = self._fetch_video_url(word_dict)
                    log.debug(
                        f"matched {current_word_lower} in dict {dict_name} on re {regex_string}, found video {video_url}"
                    )
                    if not video_url:
                        log.error(
                            f"unable to find video url for {current_word_lower} in dict {dict_name}"
                        )
                        continue

                    equivalents = self._fetch_equivalents(word_dict)
                    signs.append(
                        Sign(
                            meaning=current_word_lower,
                            video_url=video_url,
                            equivalents=equivalents,
                            origin=self.name(),
                            additional_info={"dict_name": dict_name},
                        )
                    )

        return signs

    def _ensure_cache_exists(self):
        if not os.path.exists(self._cache_dir):
            os.makedirs(self._cache_dir)

    def _load_dicts(self):
        if self._dicts:
            return self._dicts

        self._ensure_cache_exists()

        final_dicts = {}
        for js_dict in TEGNSPROG_DICTS:
            cache_path = f"{self._cache_dir}/{js_dict}.js"
            if os.path.exists(cache_path):
                log.debug(f"cache {cache_path} exists, loading")

                words = json.loads(open(cache_path, "r").read())
            else:
                log.debug(f"cache {cache_path} does not exists, downloading")

                # fetch indeks
                url = f"{BASE_URL}/indeks/{js_dict}.js"
                r = self.s.get(url)
                words = self._parse_dict_js(r.text)

                # save content to cache
                with open(cache_path, "w") as f:
                    f.write(json.dumps(words))

            # cleanup response
            final_dicts[js_dict] = words

        # ensure the dicts are cached
        self._dicts = final_dicts

        # return dicts
        return final_dicts

    def _parse_dict_js(self, text: str) -> list[dict[str, Any]]:
        # cleanup response first
        # extract everything in the array
        text = text[text.find("Array(") + 6 : text.rfind(");")]

        words = []
        # loop over lines
        for line in text.split("\n"):
            line = line[1:-2]
            if line == "":
                continue

            lSplit = line.split("|")

            signsInt = []
            for sign in lSplit[1:]:
                signsInt.append(self._try_parse_into(sign))

            words.append(
                {
                    "word": lSplit[0],
                    "signs": signsInt,
                }
            )

        return words

    def _fetch_page(self, word_dict: dict[str, Any]) -> Optional[str]:
        if not word_dict["signs"]:
            return None

        first_sign = word_dict["signs"][0]

        # fetch sign page
        url = f"{BASE_URL}/artikler/{first_sign}.htm"
        r = self.s.get(url)
        if r.status_code == 404:
            return None

        return r.text

    def _fetch_equivalents(self, word_dict: dict[str, Any]) -> list[str]:
        req_text = self._fetch_page(word_dict)
        if not req_text:
            return []

        bs = BeautifulSoup(req_text, "html.parser")
        aekvDivs = bs.find_all("div", {"class": "aekvivalent-tekst"})

        words = []
        for aekvDiv in aekvDivs:
            for span in aekvDiv.find_all("span"):
                words.append(span.get_text().replace(",", ""))

        return words

    def _fetch_video_url(self, word_dict: dict[str, Any]) -> Optional[str]:
        req_text = self._fetch_page(word_dict)
        if not req_text:
            return None

        reg = re.compile(r"includeVideo\('\.\/video_\w\/\w_(\d+)\.htm'\)", re.UNICODE)
        matches = reg.search(req_text)
        if not matches or len(matches.groups()) != 1:
            return None

        video_id = matches.group(1)
        return f"{BASE_URL}/video/t/t_{video_id}.mp4"

    def _try_parse_into(self, maybe_int) -> Optional[int]:
        try:
            return int(maybe_int)
        except:
            return None

    def getHTTPSession(self):
        return self.s
