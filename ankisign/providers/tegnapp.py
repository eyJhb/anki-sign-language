import os
import re

import requests
from requests.exceptions import RequestException, InvalidJSONError

from ..sign import Sign

import logging

log = logging.getLogger(__name__)


# url constants
API_BASE = "http://api.tegnapp.dk/api"
API_SEARCH = API_BASE + "/search"
DOWNLOAD_URL = "http://admin.tegnapp.dk/movies_tegn/"


class Tegnapp(object):
    def __init__(self):
        self.s = requests.Session()

    def name(self):
        return "TegnApp"

    def getHTTPSession(self):
        return self.s

    def search(self, search_word: str, strict: bool = False) -> list[Sign]:
        search_word_lower = search_word.lower()

        try:
            r = self.s.get(API_SEARCH, params={"title": search_word_lower})
            words = r.json()
        except (RequestException, InvalidJSONError) as e:
            log.exception(f"failed searching for '{search_word_lower}' got error {e}")
            return []

        if len(words) == 0:
            log.debug(f"found no matches for '{search_word_lower}'")
            return []

        # compile regex for strict and non-strict usecases
        if strict:
            regex_string = r"(^%s$)" % (search_word_lower)
        else:
            regex_string = r"(^{0} | {0}$| {0} |^{0}$)".format(search_word_lower)

        regex_compiled = re.compile(regex_string, re.UNICODE)

        matched_signs = []
        for word in words:
            current_word_lower = word["Title"].lower()
            if regex_compiled.match(current_word_lower):
                meaning = word["Title"].lower()
                video_url = DOWNLOAD_URL + word["Filename"]

                log.debug(
                    f"matched {current_word_lower} on re {regex_string}, found video {video_url}"
                )

                matched_signs.append(
                    Sign(
                        meaning=meaning,
                        video_url=video_url,
                        equivalents=[],
                        origin=self.name(),
                    )
                )

        return matched_signs
