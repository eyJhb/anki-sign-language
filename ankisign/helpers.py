import os
import shutil
import subprocess
import requests

from typing import Optional

import logging

log = logging.getLogger(__name__)


def download_convert(video_url: str, cache_dir: str) -> Optional[str]:
    # get session from provider
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:106.0) Gecko/20100101 Firefox/106.0"
    }

    video_cache_dir = f"{cache_dir}/videos"

    # ensure cachedir exists
    if not os.path.exists(video_cache_dir):
        os.makedirs(video_cache_dir)

    # get filename
    filename = video_url.rsplit("/", 1)[1]

    # final path
    output_path = f"{video_cache_dir}/{filename}"

    if os.path.exists(output_path):
        return convert_file(output_path)

    r = requests.get(video_url, headers=headers, stream=True)
    if r.status_code == 200:
        with open(output_path, "wb") as f:
            r.raw.decode_content = True
            shutil.copyfileobj(r.raw, f)
    else:
        log.error(f"bad status code when downloading file: {r.status_code}")
        return None

    return convert_file(output_path)


def convert_file(path) -> str:
    # replace last part of path with .mp4
    newPath = path.rsplit(".", 1)[0] + ".mp4"

    # check if there is need for conversion
    if path == newPath:
        return newPath

    # check if path exists
    if os.path.exists(newPath):
        return newPath

    p = subprocess.call(
        [
            "ffmpeg",
            "-i",
            path,
            newPath,
        ]
    )

    return newPath
