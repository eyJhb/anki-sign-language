from typing import Optional

from .exceptions import WordlistError
from .word import Word

import logging

log = logging.getLogger(__name__)


class Wordlist:
    def __init__(self, wordlist_path: str):
        self._wordlist_path = wordlist_path

        self.strict: bool = False
        self.deck_title: str = ""
        self.deck_id: int = 0
        self.words: list[Word] = []

    def parse(self):
        file_lines = self._read_file()

        # parse meta line
        self.deck_title, self.deck_id, self.strict = self._parse_meta_line(
            file_lines.pop(0)
        )

        # parse words
        self.words = []
        for line in file_lines:
            result = self._parse_word(line)
            word, search_term = self._parse_word(line)
            if word and search_term:
                self.words.append(Word(word, search_term))

    def _read_file(self) -> list[str]:
        with open(self._wordlist_path, "r") as f:
            return f.read().split("\n")

    def _parse_word(self, word_line: str) -> tuple[Optional[str], Optional[str]]:
        if "#" in word_line or word_line.strip() == "":
            return None, None

        if ";" in word_line:
            word_line_split = word_line.split(";")
            return word_line_split[0].strip(), word_line_split[1].strip()

        return word_line.strip(), word_line.strip()

    def _parse_meta_line(self, meta_line: str) -> tuple[str, int, bool]:
        if not meta_line.startswith("#"):
            raise Exception("wordlist does not contain any metadata")

        meta_line_split = meta_line[1:].split(";")
        if not len(meta_line_split) >= 2:
            raise WordlistError(
                "metedata line format should be '# Name of deck; <deck-id>; strict (optional)"
            )

        deck_title = meta_line_split[0].strip()

        try:
            deck_id = int(meta_line_split[1].strip())
        except ValueError:
            raise WordlistError("deck-id is not a valid integer")

        # check if strict
        strict = False
        if len(meta_line_split) >= 3:
            if "strict" in meta_line_split[2].strip().lower():
                strict = True

        log.debug(f"loaded wordlist {deck_title} with id {deck_id} and strict {strict}")

        return (deck_title, deck_id, strict)
