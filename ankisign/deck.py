import genanki
import os
import shutil

from .helpers import download_convert
from .sign import Sign

from typing import Any


class Deck:
    def __init__(
        self,
        deck_title: str,
        deck_id: int,
        signs: list[Sign],
        cache_dir: str,
        model_id: int = 1465525613,
    ):
        # setup vars
        self.deck_title = deck_title
        self.deck_id = deck_id
        self.signs = signs
        self.model_id = model_id

        # private cache dir var
        self._cache_dir = cache_dir

    def generate(self, output_file: str) -> str:
        # model used to make the deck
        ankiModel = genanki.Model(
            self.model_id,
            "Sign Language Model",
            fields=[
                {"name": "Question"},
                {"name": "SignVideo"},
                {"name": "Equivalents"},
                {"name": "EnsureEmbedded"},
            ],
            templates=[
                {
                    "name": "{{Question}}",
                    "qfmt": """
                    <center><h1><b>{{Question}}</b></h1></center>
                """,
                    "afmt": """
                    {{FrontSide}}

                    <hr id="answer">

                    <center>
                    <video height="480" controls autoplay loop muted>
                        <source src="{{SignVideo}}" />
                        <p>Your browser doesn't support HTML video.</p>
                    </video>
                    <br>
                    Tilsvarende Ord: <br>
                    {{Equivalents}}
                    </center>
                """,
                },
            ],
        )

        deck = genanki.Deck(
            deck_id=self.deck_id,
            name=self.deck_title,
        )

        files = []
        for sign in self.signs:
            # download file
            videofile = download_convert(sign.video_url, cache_dir=self._cache_dir)
            if not videofile:
                raise Exception("unable to download and convert video file")

            # append to files, for embedding
            files.append(videofile)

            # final name
            finalName = videofile.rsplit("/", 1)[1]

            deck.add_note(
                SignNote(
                    model=ankiModel,
                    fields=[
                        sign.meaning,
                        finalName,
                        "<br>".join(sign.equivalents),
                        "[sound:%s]" % finalName,
                    ],
                )
            )

        pkg = genanki.Package(deck)
        pkg.media_files = files

        final_output_file = f"{output_file}.apkg"
        pkg.write_to_file(final_output_file)

        return final_output_file


# only generate the GUID based on the input word
class SignNote(genanki.Note):
    @property
    def guid(self):
        return genanki.guid_for(self.fields[0])
