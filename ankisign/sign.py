from typing import Optional, Any


class Sign:
    def __init__(
        self,
        meaning: str,
        video_url: str,
        equivalents: list[str],
        origin: str,
        additional_info: Optional[dict[str, Any]] = None,
    ):
        self.meaning = meaning
        self.video_url = video_url
        self.equivalents = equivalents
        self.origin = origin

        # mostly used for debug
        self.additional_info = additional_info
        if not additional_info:
            self.additional_info = additional_info

    def full_repr(self):
        return f"Sign({self.meaning}, {self.origin}, {self.additional_info}, {self.video_url}, {self.equivalents})"

    def __repr__(self):
        return f"Sign({self.meaning}, {self.origin}, {self.additional_info})"
