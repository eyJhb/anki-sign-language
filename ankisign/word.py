from typing import Optional


class Word:
    def __init__(self, word: str, search_term: Optional[str] = None):
        self.word = word
        self.search_term = search_term if search_term else word
