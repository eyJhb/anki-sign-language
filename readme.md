# AnkiSign
This project aids in creating Anki decks for Danish Sign Language (DTS), using a set of providers.
Currently, there are two providers available:

- [Tegnsprog.dk](https://tegnapp.dk) 
- [Tegnapp](https://play.google.com/store/apps/details?id=dk.matrialecentret.tegnapp) as backends.

Tegnsprog.dk is preferred, as it is the most complete collection of DTS, however, might contain less up-to-date words than Tegnapp, which has a number of slangs.

## Using AnkiSign
First make a file in wordlists name `wordlists/colours.list`, and open it.
The first line has the format `# <deck-name>; <deck-id>`, and is the metadata of list.
The deck name is what the deck will show up as in Anki, ie. in this case it could be `DTS - Colours`.
The deck ID is a unique ID that should persists between generating this deck, and can be generated using e.g. `import time; int(time.time())` in Python.

The next lines are the words, that AnkiSign should search for using the providers (can be multiple, but the first match returned will be used), and find the corresponding vide for that word.
Some providers might include the different meanings for a sign (ie. Tegnsprog.dk will), others will not.

The final file would then look something like the following:

```
# DTS - Colours; 1676821973
rød
blå
gul
grøn
brun
hvid
sort
```

The deck can then be generated using:

```bash
$ python -m ankisign.ankisign --wordlist wordlists/colours.list --output colours
```

Which will generate `colours.apkg`, and can be imported into Anki!

## Advanced usage
Some advance usages will be shown below.

### Strict search
Firstly the metadata has a third parameter, which is used to indicate if the search should be strict.
If strict is specified, then only the exact match of the word in the wordlist will be matched.
Ie. if the search is for `test` and the provider returns `teste`, then it will not be returned (which might very well be the same sign).
Adding strict to the previous example would look like:

```
# DTS - Colours; 1676821973; strict
rød
blå
gul
grøn
brun
hvid
sort
```

### Alternative search
Sometimes a word that is wanted, appears under a different search term, but you still want a specific word to show up in the deck.
For this, an alternative search term can be specified using `;`, which has the form as `<word>;<search-term>`, ie. `10;ti` would search for `ti` but show `10` in the deck.

This could look like:

```
# DTS - Colours; 1676821973; strict
red;rød
blue;blå
yellow;gul
green;grøn
brown;brun
white;hvid
black;sort
```

## Special notes
Each note (flashcard) in Anki has a unique ID, which Anki uses to identify the different notes.
When importing a deck, this ID is used to identify if a new note should be added, or a note should be updated.

Normally with `genanki`, this note ID is generated based on all the fields on a note, which in this case would include the video file.
This is however not wanted, as sometimes a sign video might be wrong, and should be updated without removing the entire deck.
Therefore, the ID is instead based on the `<word>`, ie. `red`.

Enjoy the project! If any bugs are found, please feel free to open an issue.
